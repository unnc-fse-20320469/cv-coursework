# CV coursework



## Getting started

1.Installing Python dependencies
pip install -r requirements.txt

2.Installing Pytorch
(Preferably a GPU version, otherwise SwinIR inference will be slow)
Test whether it is a GPU version: 
import torch
print(torch.cuda.is_available()) # should be True

3.Installing Qt video decoder
For Windows: https://github.com/Nevcairiel/LAVFilters/releases/download/0.79.2/LAVFilters-0.79.2-Installer.exe 

4.Note: The video moves from left to right

5. Using the GUI
Run gui.py directly
The result image is saved to the optional path parameter finish_progress in gui.py.

def finish_progress(self, result, path='data/output.png'):
    cv2.imwrite(path, result)
    self.image_window = ImageDisplayWindow(path)
    self.image_window.show()

6.Not using the GUI
Run panorama_generator.py

if __name__ == '__main__':
    generator = PanoramaGenerator(SIFTDetector(),
                                  BFMatcher(),
                                  ColorTransferStitcher(),
                                  SimplePreProcessor(),
                                  [SimplePostProcessor(), SwinIRPostProcessor()])
   
    result = generator.generate('data/video.mp4', dst='data/output.png', frame_rate=15)

Change video.mp4 name  becoming the video name you want to use
Change the output.png of the output image name
Frame rate refers to how many frames are taken out for stitching.
When creating the PanoramaGenerator object, just pass in the subclasses you want to use for the corresponding stage. 
For example, if you want to use the ORB feature detector during the feature detection phase, just change the SIFTDetector to ORBDetector in the code

7.data package has the test videos and output
  models package is the SwinIR Denoising Model
